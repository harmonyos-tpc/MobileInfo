package com.mobile.mobileinfo.view.page;

import com.mobile.mobilehardware.base.BaseData;
import com.mobile.mobileinfo.data.Param;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static com.mobile.mobileinfo.view.page.Util.getParamValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class DebugInfoOhosTest {
    DebugInfo mdebugInfo;
    List<Param> list;

    @Before
    public void setUp() {
        mdebugInfo = new DebugInfo();
    }

    @Test
    public void getName() {
        assertEquals("DebugInfo", mdebugInfo.getName());
    }

    @Test
    public void getList() {
        list = mdebugInfo.getList();
        assertTrue(getParamValue(list, BaseData.Debug.IS_DEBUG_VERSION).length() > 0);
        assertTrue(getParamValue(list, BaseData.Debug.IS_DEBUGGING).length() > 0);
        assertTrue(getParamValue(list, BaseData.Debug.IS_READ_PROC_STATUS).length() > 0);
    }
}