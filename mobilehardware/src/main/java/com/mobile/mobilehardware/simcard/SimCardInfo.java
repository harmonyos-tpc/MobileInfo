package com.mobile.mobilehardware.simcard;

import com.mobile.mobilehardware.utils.Logs;
import ohos.app.Context;
import ohos.telephony.SimInfoManager;
import ohos.utils.zson.ZSONObject;

import static com.mobile.mobilehardware.simcard.SimCardUtils.getActiveSIMSlot;
import static ohos.telephony.TelephonyConstants.SIM_STATE_NOT_PRESENT;
import static ohos.telephony.TelephonyConstants.SIM_STATE_UNKNOWN;

/**
 * @author guxiaonian
 */
class SimCardInfo {
    private static final String TAG = SimCardInfo.class.getSimpleName();


    static ZSONObject getMobSimInfo(Context context) {
        SimCardBean simCardBean = new SimCardBean();
        try {
            simCardBean.setHaveCard(hasSimCard(context));
            MobCardUtils.mobGetCardInfo(context, simCardBean);
        } catch (Exception e) {
            Logs.e(TAG, e.toString());
        }
        return simCardBean.toJSONObject();

    }

    /**
     * 判断是否包含SIM卡
     *
     * @param context 上下文
     * @return 状态 是否包含SIM卡
     */
    private static boolean hasSimCard(Context context) {
        switch (SimInfoManager.getInstance(context).getSimState(getActiveSIMSlot(context))) {
            case SIM_STATE_NOT_PRESENT:
            case SIM_STATE_UNKNOWN:
                return false;
        }
        return true;
    }
    /**
     * 判断是否包含SIM卡
     *
     * @param context 上下文
     * @param slot sim slot 0 or 1
     * @return 状态 是否包含SIM卡
     */
    public static boolean hasSimCardInSlot(Context context, int slot) {
        switch (SimInfoManager.getInstance(context).getSimState(slot)) {
            case SIM_STATE_NOT_PRESENT:
            case SIM_STATE_UNKNOWN:
                return false;
        }
        return true;
    }
}
