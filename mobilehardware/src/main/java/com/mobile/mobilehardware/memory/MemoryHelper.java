package com.mobile.mobilehardware.memory;

import com.mobile.mobilehardware.MobileHardWareHelper;
import ohos.utils.zson.ZSONObject;

/**
 * @author guxiaonian
 */
public class MemoryHelper extends MemoryInfo {

    /**
     * info
     *
     * @return memory info
     */
    public static ZSONObject getMemoryInfo() {
        return memoryInfo(MobileHardWareHelper.getContext());
    }
}
