package com.mobile.mobileinfo.view.page;

import com.mobile.mobilehardware.base.BaseData;
import com.mobile.mobileinfo.data.Param;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static com.mobile.mobileinfo.view.page.Util.getParamValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class BandInfoOhosTest {
    BandInfo mBandInfo;
    List<Param> list;

    @Test
    public void getList() {
        list = mBandInfo.getList();
        assertTrue(getParamValue(list, BaseData.Band.BASE_BAND).length() > 0);
        assertTrue(getParamValue(list, BaseData.Band.INNER_BAND).length() > 0);
        assertTrue(getParamValue(list, BaseData.Band.LINUX_BAND).length() > 0);
        assertTrue(getParamValue(list, BaseData.Band.DETAIL_LINUX_BAND).length() > 0);
    }

    @Test
    public void getName() {
        assertEquals("BandInfo", mBandInfo.getName());
    }

    @Before
    public void setUp() {
        mBandInfo = new BandInfo();
    }
}