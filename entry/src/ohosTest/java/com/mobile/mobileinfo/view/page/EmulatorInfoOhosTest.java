package com.mobile.mobileinfo.view.page;

import com.mobile.mobilehardware.base.BaseData;
import com.mobile.mobileinfo.data.Param;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static com.mobile.mobileinfo.view.page.Util.getParamValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class EmulatorInfoOhosTest {
    EmulatorInfo memulatorInfo;
    List<Param> list;

    @Before
    public void setUp() {
        memulatorInfo = new EmulatorInfo();
    }

    @Test
    public void getName() {
        assertEquals("EmulatorInfo", memulatorInfo.getName());
    }

    @Test
    public void getList() {
        list = memulatorInfo.getList();
        assertTrue(getParamValue(list, BaseData.Emulator.CHECK_BUILD).length() > 0);
        assertTrue(getParamValue(list, BaseData.Emulator.CHECK_PIPES).length() > 0);
        assertTrue(getParamValue(list, BaseData.Emulator.CHECK_QEMU_DRIVER_FILE).length() > 0);
        assertTrue(getParamValue(list, BaseData.Emulator.CHECK_CPU_INFO).length() > 0);

    }
}