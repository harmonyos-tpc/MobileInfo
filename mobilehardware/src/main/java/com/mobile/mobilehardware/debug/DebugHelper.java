package com.mobile.mobilehardware.debug;


import com.mobile.mobilehardware.MobileHardWareHelper;
import ohos.utils.zson.ZSONObject;


/**
 * @author guxiaonian
 */
public class DebugHelper extends DebugInfo {

    /**
     * 调试模式判断
     *
     * @return debug data
     */
    public static ZSONObject getDebuggingData() {
        return getDebugData(MobileHardWareHelper.getContext());
    }

}


