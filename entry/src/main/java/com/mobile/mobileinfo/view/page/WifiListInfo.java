/* * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mobile.mobileinfo.view.page;

import com.mobile.mobileinfo.data.Param;
import com.mobile.mobileinfo.view.model.Constants;
import ohos.agp.window.dialog.ToastDialog;
import ohos.eventhandler.InnerEvent;

import java.util.ArrayList;
import java.util.List;

import static com.mobile.mobileinfo.slice.MainAbilitySlice.eventHandler;

public class WifiListInfo extends AbstractPageView{
    public static ArrayList<Param> list = new ArrayList<>();

    @Override
    List<Param> getList() {
        if (list.size() == 0) {
            InnerEvent event = InnerEvent.get(Constants.WIFIINFO_TASK, "WifiListInfo");
            eventHandler.sendEvent(event);
            new ToastDialog(getContext())
                    .setText("Wifi Scanning Started, Please wait..")
                    .setDuration(3000)
                    .show();
        }
        return list;
    }

    public static void setList(ArrayList<Param> newvalues) {
        if (newvalues == null) {
            list.clear();
        } else {
            list = newvalues;
        }
    }
    @Override
    public String getName() {
        return "WifiListInfo";
    }
}
