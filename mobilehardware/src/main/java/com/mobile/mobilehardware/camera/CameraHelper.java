package com.mobile.mobilehardware.camera;


import com.mobile.mobilehardware.MobileHardWareHelper;
import ohos.utils.zson.ZSONObject;


/**
 * @author guxiaonian
 */
public class CameraHelper extends CameraInfo {

    /**
     * 摄像头信息
     *
     * @return camera info
     */
    public static ZSONObject getCameraInfo() {
        return cameraInfo(MobileHardWareHelper.getContext());
    }

}
