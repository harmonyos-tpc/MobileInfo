package com.mobile.mobileinfo.view.page;

import com.mobile.mobilehardware.base.BaseData;
import com.mobile.mobileinfo.data.Param;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static com.mobile.mobileinfo.view.page.Util.getParamValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class MemoryInfoOhosTest {
    MemoryInfo mMemoryInfo;
    List<Param> list;

    @Test
    public void getList() {
        list = mMemoryInfo.getList();
        assertTrue(getParamValue(list, BaseData.Memory.RAM_MEMORY).length() > 0);
        assertTrue(getParamValue(list, BaseData.Memory.RAM_AVAIL_MEMORY).length() > 0);
        assertTrue(getParamValue(list, BaseData.Memory.ROM_MEMORY_AVAILABLE).length() > 0);
        assertTrue(getParamValue(list, BaseData.Memory.ROM_MEMORY_TOTAL).length() > 0);
        assertTrue(getParamValue(list, BaseData.Memory.SDCARD_MEMORY_AVAILABLE).length() > 0);
        assertTrue(getParamValue(list, BaseData.Memory.SDCARD_MEMORY_TOTAL).length() > 0);
        assertTrue(getParamValue(list, BaseData.Memory.SDCARD_REAL_MEMORY_TOTAL).length() > 0);
    }

    @Test
    public void getName() {
        assertEquals("MemoryInfo", mMemoryInfo.getName());
    }

    @Before
    public void setUp() {
        mMemoryInfo = new MemoryInfo();
    }
}