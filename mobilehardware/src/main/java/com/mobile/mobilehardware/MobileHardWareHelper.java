package com.mobile.mobilehardware;

import com.mobile.mobilehardware.app.PackageHelper;
import com.mobile.mobilehardware.audio.AudioHelper;
import com.mobile.mobilehardware.band.BandHelper;
import com.mobile.mobilehardware.battery.BatteryHelper;
import com.mobile.mobilehardware.bluetooth.BluetoothHelper;
import com.mobile.mobilehardware.camera.CameraHelper;
import com.mobile.mobilehardware.cpu.CpuHelper;
import com.mobile.mobilehardware.debug.DebugHelper;
import com.mobile.mobilehardware.emulator.EmulatorHelper;
import com.mobile.mobilehardware.hook.HookHelper;
import com.mobile.mobilehardware.local.LocalHelper;
import com.mobile.mobilehardware.memory.MemoryHelper;
import com.mobile.mobilehardware.moreopen.MoreOpenHelper;
import com.mobile.mobilehardware.network.NetWorkHelper;
import com.mobile.mobilehardware.root.RootHelper;
import com.mobile.mobilehardware.sdcard.SDCardHelper;
import com.mobile.mobilehardware.setting.SettingsHelper;
import com.mobile.mobilehardware.signal.SignalHelper;
import com.mobile.mobilehardware.simcard.SimCardHelper;
import com.mobile.mobilehardware.stack.StackSampler;
import com.mobile.mobilehardware.useragent.UserAgentHelper;
import com.mobile.mobilehardware.wifilist.WifiHelper;
import com.mobile.mobilehardware.wifilist.WifiScanListener;
import ohos.app.Context;
import ohos.utils.zson.ZSONObject;

/**
 * @author guxiaonian
 */
public class MobileHardWareHelper {

    /**
     * 全局上下文
     */
    private static Context mContext;

    /**
     * 设置全局上下文 默认使用MobInitializer来进行初始化
     * 可以自行修改
     *
     * @param context 上下文
     */
    public static void init(Context context) {
        mContext = context;
    }

    /**
     * 获取全局上下文
     *
     * @return 上下文
     */
    public static Context getContext() {
        return mContext;
    }

    /**
     * Random文件信息
     *
     * @return @see https://github.com/guxiaonian/MobileInfo/wiki/Random
     */
    public static ZSONObject getRandomInfo() {
        return MobileNativeHelper.getRandomData();
    }


    /**
     * app包信息
     *
     * @return @see https://github.com/guxiaonian/MobileInfo/wiki/Package
     */
    public static ZSONObject getPackageInfo() {
        return PackageHelper.getPackageInfo();
    }

    /**
     * 获取手机音量的信息
     *
     * @return @see https://github.com/guxiaonian/MobileInfo/wiki/Audio
     */
    public static ZSONObject getAudioInfo() {
        return AudioHelper.mobGetMobAudio();
    }

    /**
     * 手机版本信息
     *
     * @return @see https://github.com/guxiaonian/MobileInfo/wiki/Band
     */
    public static ZSONObject getBandInfo() {
        return BandHelper.mobGetBandInfo();
    }

    /**
     * 手机电池信息
     *
     * @return @see https://github.com/guxiaonian/MobileInfo/wiki/Battery
     */
    public static ZSONObject getBatteryInfo() {
        return BatteryHelper.mobGetBattery();
    }


    /**
     * 蓝牙信息 bluetooth
     *
     * @return @see https://github.com/guxiaonian/MobileInfo/wiki/Bluetooth
     */
    public static ZSONObject getBluetoothInfo() {
        return BluetoothHelper.mobGetMobBluetooth();
    }


    /**
     * 手机摄像头信息
     *
     * @return @see https://github.com/guxiaonian/MobileInfo/wiki/Camera
     */
    public static ZSONObject getCameraInfo() {
        return CameraHelper.getCameraInfo();
    }

    /**
     * 手机CPU信息
     *
     * @return @see https://github.com/guxiaonian/MobileInfo/wiki/Cpu
     */
    public static ZSONObject getCpuInfo() {
        return CpuHelper.mobGetCpuInfo();
    }

    /**
     * 判断debug的信息
     *
     * @return @see https://github.com/guxiaonian/MobileInfo/wiki/Debug
     */
    public static ZSONObject getDebugInfo() {
        return DebugHelper.getDebuggingData();
    }

    /**
     * 手机基本信息 emulator
     *
     * @return @see https://github.com/guxiaonian/MobileInfo/wiki/Emulator
     */
    public static ZSONObject getEmulatorInfo() {
        return EmulatorHelper.mobCheckEmulator();
    }

    /**
     * 判断是否包含Hook工具
     *
     * @return @see https://github.com/guxiaonian/MobileInfo/wiki/Hook
     */
    public static ZSONObject getHookInfo() {
        return HookHelper.isXposedHook();
    }

    /**
     * 手机基本信息 local
     *
     * @return @see https://github.com/guxiaonian/MobileInfo/wiki/Local
     */
    public static ZSONObject getLocalInfo() {
        return LocalHelper.mobGetMobLocal();
    }

    /**
     * 手机内存信息
     *
     * @return @see https://github.com/guxiaonian/MobileInfo/wiki/Memory
     */
    public static ZSONObject getMemoryInfo() {
        return MemoryHelper.getMemoryInfo();
    }

    /**
     * 判断多开软件的信息
     *
     * @return @see https://github.com/guxiaonian/MobileInfo/wiki/MoreOpen
     */
    public static ZSONObject getMoreOpenInfo() {
        return MoreOpenHelper.checkVirtual();
    }

    /**
     * 手机基本信息 netWork
     *
     * @return @see https://github.com/guxiaonian/MobileInfo/wiki/NetWork
     */
    public static ZSONObject getNetWorkInfo() {
        return NetWorkHelper.mobGetMobNetWork();
    }


    /**
     * 手机是否root
     *
     * @return @see https://github.com/guxiaonian/MobileInfo/wiki/Root
     */
    public static boolean isRoot() {
        return RootHelper.mobileRoot();
    }


    /**
     * 获取SDCard信息
     *
     * @return @see https://github.com/guxiaonian/MobileInfo/wiki/SDCard
     */
    public static ZSONObject getSDCardInfo() {
        return SDCardHelper.mobGetSdCard();
    }

    /**
     * 手机设置信息
     *
     * @return @see https://github.com/guxiaonian/MobileInfo/wiki/Settings
     */
    public static ZSONObject getSettingsInfo() {
        return SettingsHelper.mobGetMobSettings();
    }

    /**
     * 手机信号信息
     *
     * @return @see https://github.com/guxiaonian/MobileInfo/wiki/Signal
     */
    public static ZSONObject getSignalInfo() {
        return SignalHelper.mobGetNetRssi();
    }

    /**
     * 返回默认的UA
     *
     * @return @see https://github.com/guxiaonian/MobileInfo/wiki/UserAgent
     */
    public static String getUserAgentInfo() {
        return UserAgentHelper.getDefaultUserAgent();
    }

    /**
     * 判断当前线程的堆栈信息
     *
     * @param thread 所要获取的线程
     * @return @see https://github.com/guxiaonian/MobileInfo/wiki/Stack
     */
    public static String getThreadStackInfo(Thread thread) {
        return StackSampler.getStackInfo(thread);
    }

    /**
     * 拿到手机卡的详细信息
     *
     * @return @see https://github.com/guxiaonian/MobileInfo/wiki/SimCard
     */
    public static ZSONObject getSimInfo() {
        return SimCardHelper.mobileSimInfo();
    }

    /**
     * 获取WIFI列表清单文件
     * @param wifiScanListener scan listener
     * @return @see https://github.com/guxiaonian/MobileInfo/wiki/WifiList
     */
    public static void getWifiListInfo(WifiScanListener wifiScanListener) {
        WifiHelper.wifiList(wifiScanListener);
    }
}
