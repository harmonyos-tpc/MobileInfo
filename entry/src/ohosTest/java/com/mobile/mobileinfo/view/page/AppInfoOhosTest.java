package com.mobile.mobileinfo.view.page;

import com.mobile.mobilehardware.base.BaseData;
import com.mobile.mobileinfo.data.Param;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static com.mobile.mobileinfo.view.page.Util.getParamValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class AppInfoOhosTest {
    AppInfo mAppInfo;
    List<Param> list;

    @Test
    public void getList() {
        list = mAppInfo.getList();
        assertTrue(getParamValue(list, BaseData.App.APP_NAME).length() > 0);
        assertTrue(getParamValue(list, BaseData.App.LAST_UPDATE_TIME).length() > 0);
        assertTrue(getParamValue(list, BaseData.App.FIRST_INSTALL_TIME).length() > 0);
        assertEquals("com.mobile.mobileinfo", getParamValue(list, BaseData.App.PACKAGE_NAME));
        assertTrue(getParamValue(list, BaseData.App.APP_VERSION_CODE).length() > 0);
        assertTrue(getParamValue(list, BaseData.App.APP_VERSION_NAME).length() > 0);
        assertTrue(getParamValue(list, BaseData.App.APP_TARGET_SDK_VERSION).length() > 0);
        assertTrue(getParamValue(list, BaseData.App.APP_MIN_SDK_VERSION).length() > 0);
        assertTrue(getParamValue(list, BaseData.App.APP_DESCRIPTION).length() > 0);
        assertTrue(getParamValue(list, BaseData.App.APP_ICON).length() > 0);
    }

    @Test
    public void getName() {
        assertEquals("AppInfo", mAppInfo.getName());
    }

    @Before
    public void setUp() {
        mAppInfo = new AppInfo();
    }
}