package com.mobile.mobilehardware.wifilist;

import ohos.utils.zson.ZSONObject;

/**
 * @author guxiaonian
 */
public interface WifiScanListener {
    void onResult(ZSONObject jsonObject);
}
