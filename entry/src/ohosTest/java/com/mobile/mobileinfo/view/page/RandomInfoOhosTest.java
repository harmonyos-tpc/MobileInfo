package com.mobile.mobileinfo.view.page;

import com.mobile.mobileinfo.data.Param;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static com.mobile.mobileinfo.view.page.Util.getParamValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class RandomInfoOhosTest {
    RandomInfo mRandomInfo;
    List<Param> list;

    @Test
    public void getList() {
        list = mRandomInfo.getList();
        assertTrue(getParamValue(list, "bootId").length() > 0);
        assertTrue(getParamValue(list, "entropyAvail").length() > 0);
        assertTrue(getParamValue(list, "poolSize").length() > 0);
        assertTrue(getParamValue(list, "readWakeupThreshold").length() > 0);
        assertTrue(getParamValue(list, "writeWakeupThreshold").length() > 0);
        assertTrue(getParamValue(list, "uuid").length() > 0);
        assertTrue(getParamValue(list, "uRandomMinReseedSecs").length() > 0);
    }

        @Test
    public void getName() {
        assertEquals("RandomInfo", mRandomInfo.getName());
    }

    @Before
    public void setUp() {
        mRandomInfo = new RandomInfo();
    }
}