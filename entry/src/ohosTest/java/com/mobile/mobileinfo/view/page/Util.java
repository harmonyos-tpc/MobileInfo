package com.mobile.mobileinfo.view.page;

import com.mobile.mobileinfo.data.Param;

import java.util.List;

public class Util {
    public static String getParamValue(List<Param> plist, String key) {
        for (Param p: plist) {
            if (p.getKey().equals(key))
                return p.getValue().toString();
        }
        return "";
    }
}
