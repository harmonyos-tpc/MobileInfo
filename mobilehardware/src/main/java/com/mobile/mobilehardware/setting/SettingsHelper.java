package com.mobile.mobilehardware.setting;


import com.mobile.mobilehardware.MobileHardWareHelper;
import ohos.utils.zson.ZSONObject;

/**
 * @author guxiaonian
 */
public class SettingsHelper extends SettingsInfo {

    /**
     * 获取设置信息
     *
     * @return json object for settings
     */
    public static ZSONObject mobGetMobSettings() {
        return getMobSettings(MobileHardWareHelper.getContext());
    }

}
