package com.mobile.mobileinfo.view.page;

import com.mobile.mobilehardware.base.BaseData;
import com.mobile.mobileinfo.data.Param;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static com.mobile.mobileinfo.view.page.Util.getParamValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class AudioInfoOhosTest {
    AudioInfo mAudioInfo;
    List<Param> list;

    @Before
    public void setUp() {
        mAudioInfo = new AudioInfo();
    }

    @Test
    public void getName() {
        assertEquals("AudioInfo", mAudioInfo.getName());
    }

    @Test
    public void getList() {
        list = mAudioInfo.getList();
        assertTrue(Integer.parseInt(getParamValue(list, BaseData.Aduio.MAX_VOICE_CALL)) >= 0);
        assertTrue(Integer.parseInt(getParamValue(list, BaseData.Aduio.MIN_VOICE_CALL)) >= 0);
        assertTrue(Integer.parseInt(getParamValue(list, BaseData.Aduio.CURRENT_VOICE_CALL)) >= 0);
        assertTrue(Integer.parseInt(getParamValue(list, BaseData.Aduio.MAX_SYSTEM)) >= 0);
        assertTrue(Integer.parseInt(getParamValue(list, BaseData.Aduio.MIN_SYSTEM)) >= 0);
        assertTrue(Integer.parseInt(getParamValue(list, BaseData.Aduio.CURRENT_SYSTEM)) >= 0);
        assertTrue(Integer.parseInt(getParamValue(list, BaseData.Aduio.MAX_RING)) >= 0);
        assertTrue(Integer.parseInt(getParamValue(list, BaseData.Aduio.MIN_RING)) >= 0);
        assertTrue(Integer.parseInt(getParamValue(list, BaseData.Aduio.CURRENT_RING)) >= 0);
        assertTrue(Integer.parseInt(getParamValue(list, BaseData.Aduio.MAX_MUSIC)) >= 0);
        assertTrue(Integer.parseInt(getParamValue(list, BaseData.Aduio.MIN_MUSIC)) >= 0);
        assertTrue(Integer.parseInt(getParamValue(list, BaseData.Aduio.CURRENT_MUSIC)) >= 0);
        assertTrue(Integer.parseInt(getParamValue(list, BaseData.Aduio.MAX_ALARM)) >= 0);
        assertTrue(Integer.parseInt(getParamValue(list, BaseData.Aduio.MIN_ALARM)) >= 0);
        assertTrue(Integer.parseInt(getParamValue(list, BaseData.Aduio.CURRENT_ALARM)) >= 0);
        assertTrue(Integer.parseInt(getParamValue(list, BaseData.Aduio.MAX_NOTIFICATIONS)) >= 0);
        assertTrue(Integer.parseInt(getParamValue(list, BaseData.Aduio.MIN_NOTIFICATIONS)) >= 0);
        assertTrue(Integer.parseInt(getParamValue(list, BaseData.Aduio.CURRENT_NOTIFICATIONS)) >= 0);
        assertTrue(Integer.parseInt(getParamValue(list, BaseData.Aduio.MAX_ACCESSIBILITY)) >= 0);
        assertTrue(Integer.parseInt(getParamValue(list, BaseData.Aduio.MIN_ACCESSIBILITY)) >= 0);
        assertTrue(Integer.parseInt(getParamValue(list, BaseData.Aduio.CURRENT_ACCESSIBILITY)) >= 0);
        assertTrue(Integer.parseInt(getParamValue(list, BaseData.Aduio.MAX_DTMF)) >= 0);
        assertTrue(Integer.parseInt(getParamValue(list, BaseData.Aduio.MIN_DTMF)) >= 0);
        assertTrue(Integer.parseInt(getParamValue(list, BaseData.Aduio.CURRENT_DTMF)) >= 0);
    }
}