package com.mobile.mobileinfo.view.page;

import com.mobile.mobilehardware.base.BaseData;
import com.mobile.mobileinfo.data.Param;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static com.mobile.mobileinfo.view.page.Util.getParamValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class NetWorkInfoOhosTest {
    NetWorkInfo mnetWorkinfo;
    List<Param> list;

    @Before
    public void setUp() {
        mnetWorkinfo = new NetWorkInfo();
    }

    @Test
    public void getName() {
        assertEquals("NetWorkInfo", mnetWorkinfo.getName());
    }

    @Test
    public void getList() {
        list = mnetWorkinfo.getList();
        assertTrue(getParamValue(list, BaseData.NetWork.TYPE).length() > 0);
        assertTrue(getParamValue(list, BaseData.NetWork.NETWORK_AVAILABLE).length() > 0);
        assertTrue(getParamValue(list, BaseData.NetWork.HAVE_INTENT).length() > 0);
        assertTrue(getParamValue(list, BaseData.NetWork.IS_FLIGHT_MODE).length() > 0);
        assertTrue(getParamValue(list, BaseData.NetWork.IS_NFC_ENABLED).length() > 0);
        assertTrue(getParamValue(list, BaseData.NetWork.IS_HOTSPOT_ENABLED).length() > 0);
        assertTrue(getParamValue(list, "isVpn").length() > 0);
    }
}