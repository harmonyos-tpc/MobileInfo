package com.mobile.mobilehardware.audio;

import com.mobile.mobilehardware.utils.Logs;
import ohos.media.audio.AudioManager;
import ohos.media.audio.AudioRemoteException;
import ohos.utils.zson.ZSONObject;

/**
 * @author guxiaonian
 */
class AudioInfo {

    private static final String TAG = AudioInfo.class.getSimpleName();

    static ZSONObject getAudio() {
        AudioBean mobAudioBean = new AudioBean();

        AudioManager audioManager = new AudioManager();

        try {
            mobAudioBean.setMaxVoiceCall(audioManager.getMaxVolume(AudioManager.AudioVolumeType.STREAM_VOICE_CALL));
            mobAudioBean.setCurrentVoiceCall(audioManager.getVolume(AudioManager.AudioVolumeType.STREAM_VOICE_CALL));

            mobAudioBean.setMaxSystem(audioManager.getMaxVolume(AudioManager.AudioVolumeType.STREAM_SYSTEM));
            mobAudioBean.setCurrentSystem(audioManager.getVolume(AudioManager.AudioVolumeType.STREAM_SYSTEM));

            mobAudioBean.setMaxRing(audioManager.getMaxVolume(AudioManager.AudioVolumeType.STREAM_RING));
            mobAudioBean.setCurrentRing(audioManager.getVolume(AudioManager.AudioVolumeType.STREAM_RING));

            mobAudioBean.setMaxMusic(audioManager.getMaxVolume(AudioManager.AudioVolumeType.STREAM_MUSIC));
            mobAudioBean.setCurrentMusic(audioManager.getVolume(AudioManager.AudioVolumeType.STREAM_MUSIC));

            mobAudioBean.setMaxAlarm(audioManager.getMaxVolume(AudioManager.AudioVolumeType.STREAM_ALARM));
            mobAudioBean.setCurrentAlarm(audioManager.getVolume(AudioManager.AudioVolumeType.STREAM_ALARM));

            mobAudioBean.setMaxNotifications(audioManager.getMaxVolume(AudioManager.AudioVolumeType.STREAM_NOTIFICATION));
            mobAudioBean.setCurrentNotifications(audioManager.getVolume(AudioManager.AudioVolumeType.STREAM_NOTIFICATION));

            mobAudioBean.setMaxDTMF(audioManager.getMaxVolume(AudioManager.AudioVolumeType.STREAM_DTMF));
            mobAudioBean.setCurrentDTMF(audioManager.getVolume(AudioManager.AudioVolumeType.STREAM_DTMF));

            mobAudioBean.setMaxAccessibility(audioManager.getMaxVolume(AudioManager.AudioVolumeType.STREAM_ACCESSIBILITY));
            mobAudioBean.setCurrentAccessibility(audioManager.getVolume(AudioManager.AudioVolumeType.STREAM_ACCESSIBILITY));

            mobAudioBean.setMinDTMF(audioManager.getMinVolume(AudioManager.AudioVolumeType.STREAM_DTMF));
            mobAudioBean.setMinNotifications(audioManager.getMinVolume(AudioManager.AudioVolumeType.STREAM_NOTIFICATION));
            mobAudioBean.setMinAlarm(audioManager.getMinVolume(AudioManager.AudioVolumeType.STREAM_ALARM));
            mobAudioBean.setMinMusic(audioManager.getMinVolume(AudioManager.AudioVolumeType.STREAM_MUSIC));
            mobAudioBean.setMinRing(audioManager.getMinVolume(AudioManager.AudioVolumeType.STREAM_RING));
            mobAudioBean.setMinSystem(audioManager.getMinVolume(AudioManager.AudioVolumeType.STREAM_SYSTEM));
            mobAudioBean.setMinVoiceCall(audioManager.getMinVolume(AudioManager.AudioVolumeType.STREAM_VOICE_CALL));
            mobAudioBean.setMinAccessibility(audioManager.getMinVolume(AudioManager.AudioVolumeType.STREAM_ACCESSIBILITY));
        } catch (AudioRemoteException e) {
            Logs.e(TAG, e.toString());
        }
        return mobAudioBean.toJSONObject();
    }
}
