package com.mobile.mobilehardware.debug;

import com.mobile.mobilehardware.base.BaseBean;
import com.mobile.mobilehardware.base.BaseData;
import com.mobile.mobilehardware.utils.Logs;
import ohos.utils.zson.ZSONObject;

/**
 * @author guxiaonian
 */
public class DebugBean extends BaseBean {
    private static final String TAG = DebugBean.class.getSimpleName();

    /**
     * 是否是Debug版本
     */
    private boolean isDebugVersion;

    /**
     * 是否正在调试
     */
    private boolean isDebugging;

    /**
     * 读取id判断是否在调试
     */
    private boolean isReadProcStatus;

    public boolean isDebugVersion() {
        return isDebugVersion;
    }

    public void setDebugVersion(boolean debugVersion) {
        isDebugVersion = debugVersion;
    }

    public boolean isDebugging() {
        return isDebugging;
    }

    public void setDebugging(boolean debugging) {
        isDebugging = debugging;
    }

    public boolean isReadProcStatus() {
        return isReadProcStatus;
    }

    public void setReadProcStatus(boolean readProcStatus) {
        isReadProcStatus = readProcStatus;
    }


    @Override
    protected ZSONObject toJSONObject() {
        try {
            jsonObject.put(BaseData.Debug.IS_DEBUG_VERSION, isDebugVersion);
            jsonObject.put(BaseData.Debug.IS_DEBUGGING, isDebugging);
            jsonObject.put(BaseData.Debug.IS_READ_PROC_STATUS, isReadProcStatus);
        } catch (Exception e) {
            Logs.e(TAG, e.toString());
        }
        return super.toJSONObject();
    }
}
