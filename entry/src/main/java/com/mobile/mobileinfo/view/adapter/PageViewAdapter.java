/*
 *  * Copyright (C) 2021 Huawei Device Co., Ltd.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 * /
 */

package com.mobile.mobileinfo.view.adapter;

import com.mobile.mobileinfo.view.model.PageInfo;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.PageSliderProvider;
import ohos.agp.components.StackLayout;
import ohos.agp.window.service.Window;

import java.util.List;

/**
 * The type Page view adapter.
 */
public class PageViewAdapter extends PageSliderProvider {
    private static final String TAG = PageViewAdapter.class.getCanonicalName();
    private List<? extends PageInfo> pageViews;
    private static Window mWindow;

    /**
     * Instantiates a new Page view adapter.
     *
     * @param list         the list
     */
    public PageViewAdapter(List<? extends PageInfo> list) {
        pageViews = list;
    }

    public void setWindow(Window window) {
        mWindow = window;
    }

    /**
     * isPageMatchToObject
     *
     * @param component The Component
     * @param obj The Object
     * @return boolean
     */
    @Override
    public boolean isPageMatchToObject(Component component, Object obj) {
        return true;
    }

    /**
     * gets Adapter Count
     *
     * @return count
     */
    public int getCount() {
        return this.pageViews.size();
    }

    /**
     * creates Individual Pages
     *
     * @param componentContainer the ComponentContainer
     * @param position the Current Position
     * @return Object
     */
    public Object createPageInContainer(ComponentContainer componentContainer, int position) {
        Component mobileinfopage = pageViews.get(position).getRootView(componentContainer.getContext(), mWindow);
        mobileinfopage.setLayoutConfig(new StackLayout.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_PARENT, ComponentContainer.LayoutConfig.MATCH_PARENT));
        componentContainer.addComponent(mobileinfopage);
        return mobileinfopage;
    }

    /**
     * Destroys Pages
     *
     * @param componentContainer the ComponentContainer
     * @param position the Current Position
     * @param obj Object
     */
    public void destroyPageFromContainer(ComponentContainer componentContainer, int position, Object obj) {
        if (componentContainer != null && (obj instanceof Component)) {
            componentContainer.removeComponent((Component) obj);
        }
    }
}
