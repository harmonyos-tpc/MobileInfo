package com.mobile.mobilehardware.signal;

import com.mobile.mobilehardware.utils.DataUtil;
import com.mobile.mobilehardware.utils.Logs;
import com.mobile.mobilehardware.utils.TextUtil;
import ohos.app.Context;
import ohos.net.NetManager;
import ohos.telephony.CellularDataInfoManager;
import ohos.telephony.RadioInfoManager;
import ohos.telephony.SignalInformation;
import ohos.utils.zson.ZSONObject;
import ohos.wifi.WifiDevice;
import ohos.wifi.WifiLinkedInfo;

import java.util.List;
import java.util.Optional;

/**
 * @author guxiaonian
 */
class SignalInfo {

    private static final String WIFI = "WIFI";
    private static final String TAG = SignalInfo.class.getSimpleName();


    /**
     * 信号强度获取
     *
     * @param context Application context
     * @return fill json object and return
     */
    static ZSONObject getNetRssi(Context context) {
        SignalBean signalBean = new SignalBean();
        try {
            String netWorkType = DataUtil.networkTypeALL(context);
            signalBean.setType(netWorkType);
            if (WIFI.equals(netWorkType)) {
                getDetailsWifiInfo(context, signalBean);
            } else {
                getMobileDbm(context, signalBean);
            }
        } catch (Exception e) {
            Logs.e(TAG, e.toString());
        }
        return signalBean.toJSONObject();


    }

    /**
     * mobile
     *
     * @param context Application context
     * @param signalBean Bean to store values
     * @return get mobile dbm
     */
    private static void getMobileDbm(Context context, SignalBean signalBean) {
        int level = 0;
        try {
            signalBean.setnIpAddress(getNetIPV4(context));
            RadioInfoManager tm = RadioInfoManager.getInstance(context);
            if (tm == null) {
                return;
            }
            List<SignalInformation> cellInfoList = tm.getSignalInfoList(CellularDataInfoManager.getInstance(context).getDefaultCellularDataSlotId());
            if (null != cellInfoList) {
                for (SignalInformation cellInfo : cellInfoList) {
                    level = cellInfo.getSignalLevel();
                    if (level > 0)
                        break;
                }
            }
            signalBean.setLevel(level);
        } catch (Exception e) {
            Logs.i(TAG, e.toString());
        }
    }

    private static String getNetIPV4(Context context) {
        NetManager netManager = NetManager.getInstance(context);
        if (netManager == null)
            return null;
        return netManager.getConnectionProperties(netManager.getDefaultNet()).getLinkAddresses().get(0).getAddress().toString().replace("/", "");
    }

    /**
     * Anything worse than or equal to this will show 0 bars.
     */
    private static final int MIN_RSSI = -100;

    /**
     * Anything better than or equal to this will show the max bars.
     */
    private static final int MAX_RSSI = -55;

    private static int calculateSignalLevel(int rssi) {

        if (rssi <= MIN_RSSI) {
            return 0;
        } else if (rssi >= MAX_RSSI) {
            return 4;
        } else {
            float inputRange = (MAX_RSSI - MIN_RSSI);
            float outputRange = (4);
            return (int) ((float) (rssi - MIN_RSSI) * outputRange / inputRange);
        }
    }

    /**
     * 获取WifiInfo
     *
     * @param mContext Application Context
     * @return wifo info
     */
    private static Optional<WifiLinkedInfo> getWifiInfo(Context mContext) {
        WifiDevice mWifiManager = WifiDevice.getInstance(mContext);
        if (mWifiManager != null) {
            return mWifiManager.getLinkedInfo();
        }
        return null;
    }

    /**
     * 是否使用代理上网
     *
     * @param context
     * @return
     */
    private static void isWifiProxy(SignalBean signalBean) {
// 是否大于等于4.0
        String proxyAddress;
        int proxyPort;
        proxyAddress = System.getProperty("http.proxyHost");
        String portStr = System.getProperty("http.proxyPort");
        proxyPort = Integer.parseInt((portStr != null ? portStr : "-1"));
        if ((!TextUtil.isEmpty(proxyAddress)) && (proxyPort != -1)) {
            signalBean.setProxy(true);
            signalBean.setProxyAddress(proxyAddress);
            signalBean.setProxyPort(proxyPort + "");
        } else {
            signalBean.setProxy(false);
        }
    }

    /**
     * wifi
     *
     * @param mContext application context
     * @param signalBean Bean to store settings
     * @return wifi info details
     */
    private static void getDetailsWifiInfo(Context mContext, SignalBean signalBean) {
        try {
            WifiDevice wifiDevice = WifiDevice.getInstance(mContext);
            WifiLinkedInfo mWifiInfo = wifiDevice.getLinkedInfo().get();
            int ip = mWifiInfo.getIpAddress();
            String strIp = "" + (ip & 0xFF) + "." + ((ip >> 8) & 0xFF) + "." + ((ip >> 16) & 0xFF) + "." + ((ip >> 24) & 0xFF);
            signalBean.setBssid(mWifiInfo.getBssid());
            signalBean.setSsid(mWifiInfo.getSsid().replace("\"", ""));
            signalBean.setnIpAddress(strIp);
            signalBean.setMacAddress(mWifiInfo.getMacAddress());
            signalBean.setLinkSpeed(mWifiInfo.getLinkSpeed() + "Mbps");
            int rssi = mWifiInfo.getRssi();
            signalBean.setRssi(rssi);
            signalBean.setLevel(calculateSignalLevel(rssi));
            isWifiProxy(signalBean);
            signalBean.setSupplicantState(mWifiInfo.getConnState().name());
        } catch (Exception e) {
            Logs.i(TAG, e.toString());
        }

    }


 
}
