package com.mobile.mobilehardware.screen;

import com.mobile.mobilehardware.MobileHardWareHelper;

import ohos.agp.window.service.Window;
import ohos.utils.zson.ZSONObject;

/**
 * @author guxiaonian
 */
public class ScreenHelper extends ScreenInfo{

    /**
     * 获取屏幕信息
     * @param window Window of session
     * @return screen settings json object
     */
    public static ZSONObject mobGetMobScreen(Window window) {
        return getMobScreen(MobileHardWareHelper.getContext(),window);
    }



}