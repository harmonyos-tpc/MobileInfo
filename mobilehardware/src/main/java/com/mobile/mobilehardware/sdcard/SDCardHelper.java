package com.mobile.mobilehardware.sdcard;


import com.mobile.mobilehardware.MobileHardWareHelper;
import ohos.utils.zson.ZSONObject;

/**
 * @author guxiaonian
 */
public class SDCardHelper extends SDCardInfo {

    /**
     * 获取卡信息
     *
     * @return Json object for sdcard
     */
    public static ZSONObject mobGetSdCard() {
        return getSdCard(MobileHardWareHelper.getContext());
    }

}