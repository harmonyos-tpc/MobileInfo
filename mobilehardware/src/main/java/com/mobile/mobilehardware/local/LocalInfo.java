package com.mobile.mobilehardware.local;

import com.mobile.mobilehardware.utils.Logs;
import ohos.utils.zson.ZSONObject;

import java.util.Locale;

/**
 * @author guxiaonian
 */
class LocalInfo {

    public static final String TAG = LocalInfo.class.getSimpleName();

    static ZSONObject getMobLocal() {
        LocalBean localBean = new LocalBean();
        try {
            localBean.setCountry(Locale.getDefault().getCountry());
            localBean.setLanguage(Locale.getDefault().getLanguage());
        } catch (Exception e) {
            Logs.e(TAG, e.toString());
        }
        return localBean.toJSONObject();
    }
}
