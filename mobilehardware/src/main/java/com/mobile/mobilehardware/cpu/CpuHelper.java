package com.mobile.mobilehardware.cpu;

import ohos.utils.zson.ZSONObject;

public class CpuHelper extends CpuInfo {
    /**
     * CPU
     *
     * @return cpu data
     */
    public static ZSONObject mobGetCpuInfo() {
        return getCpuInfo();
    }
}
