package com.mobile.mobilehardware.band;


import ohos.utils.zson.ZSONObject;


/**
 * @author guxiaonian
 */
public class BandHelper extends BandInfo {

    /**
     * bandInfo
     *
     * @return Band info
     */
    public static ZSONObject mobGetBandInfo() {
        return getBandInfo();
    }


}
