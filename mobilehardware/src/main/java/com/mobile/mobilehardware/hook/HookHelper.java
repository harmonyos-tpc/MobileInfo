package com.mobile.mobilehardware.hook;


import com.mobile.mobilehardware.MobileHardWareHelper;
import ohos.utils.zson.ZSONObject;


/**
 * @author guxiaonian
 */
public class HookHelper extends HookInfo {


    /**
     * 判断是否有xposed等hook工具
     *
     * @return json object with xposedvalues
     */
    public static ZSONObject isXposedHook() {
        return getXposedHook(MobileHardWareHelper.getContext());
    }


}
