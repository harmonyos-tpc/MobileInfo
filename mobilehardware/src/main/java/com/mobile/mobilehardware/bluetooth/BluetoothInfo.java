package com.mobile.mobilehardware.bluetooth;

import com.mobile.mobilehardware.utils.Logs;
import ohos.app.Context;
import ohos.bluetooth.BluetoothHost;
import ohos.bluetooth.BluetoothRemoteDevice;
import ohos.utils.zson.ZSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * @author guxiaonian
 */
class BluetoothInfo {
    private static final String TAG = BluetoothInfo.class.getSimpleName();

    static ZSONObject getMobBluetooth(Context context) {
        BluetoothBean bluetoothBean = new BluetoothBean();
        try {
            BluetoothHost bluetoothAdapter = BluetoothHost.getDefaultHost(context);
            if (bluetoothAdapter == null) {
                return bluetoothBean.toJSONObject();
            }
            bluetoothBean.setEnabled(bluetoothAdapter.getBtState() != BluetoothHost.STATE_OFF);
            bluetoothBean.setPhoneName(bluetoothAdapter.getLocalName().orElse(""));
            List<BluetoothRemoteDevice> pairedDevices = bluetoothAdapter.getPairedDevices();
            List<ZSONObject> list = new ArrayList<>();
            if (pairedDevices.size() > 0) {
                for (BluetoothRemoteDevice device : pairedDevices) {
                    BluetoothBean.DeviceBean deviceBean = new BluetoothBean.DeviceBean();
                    deviceBean.setAddress(device.getDeviceAddr());
                    deviceBean.setName(device.getDeviceName().orElse(""));
                    list.add(deviceBean.toJSONObject());
                }
            }
            bluetoothBean.setDevice(list);
        } catch (Exception e) {
            Logs.e(TAG, e.toString());
        }
        return bluetoothBean.toJSONObject();
    }
}
