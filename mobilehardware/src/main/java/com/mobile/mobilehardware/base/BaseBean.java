package com.mobile.mobilehardware.base;

import ohos.utils.zson.ZSONObject;

import java.io.Serializable;

/**
 * @author guxiaonian
 */
public class BaseBean implements Serializable {

    protected ZSONObject jsonObject = new ZSONObject();


    protected ZSONObject toJSONObject() {
        return jsonObject;
    }

    protected BaseBean() {

    }

    protected String isEmpty(String value) {
        if (value == null || value.length() == 0) {
            return BaseData.UNKNOWN_PARAM;
        }
        return value;
    }

    protected String isEmpty(CharSequence value) {
        if (value == null) {
            return BaseData.UNKNOWN_PARAM;
        }
        return value.toString();
    }
}
