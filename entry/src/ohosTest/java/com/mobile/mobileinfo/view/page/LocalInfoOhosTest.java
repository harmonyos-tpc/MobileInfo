package com.mobile.mobileinfo.view.page;

import com.mobile.mobilehardware.base.BaseData;
import com.mobile.mobileinfo.data.Param;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static com.mobile.mobileinfo.view.page.Util.getParamValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class LocalInfoOhosTest {
    LocalInfo mLocalInfo;
    List<Param> list;

    @Test
    public void getList() {
        list = mLocalInfo.getList();
        assertTrue(getParamValue(list, BaseData.Local.COUNTRY).length() > 0);
        assertTrue(getParamValue(list, BaseData.Local.LANGUAGE).length() > 0);
    }

    @Test
    public void getName() {
        assertEquals("LocalInfo", mLocalInfo.getName());
    }

    @Before
    public void setUp() {
        mLocalInfo = new LocalInfo();
    }
}