/*
 *  * Copyright (C) 2021 Huawei Device Co., Ltd.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 * /
 */

package com.mobile.mobileinfo.view.tab;

import com.mobile.mobilehardware.utils.Logs;
import ohos.aafwk.ability.AbilitySlice;
import ohos.agp.components.PageSlider;
import ohos.agp.components.TabList;

import java.util.List;
import java.util.Optional;


/**
 * The type Tab list listener.
 */
public class TabListListener implements TabList.TabSelectedListener {
    private static final String TAG = TabListListener.class.getCanonicalName();
    private PageSlider pager;
    private List<? extends TabInfo> tabViews;

    /**
     * Instantiates a new Tab list listener.
     *
     * @param abilitySlice the ability slice
     * @param list         the list
     * @param pageSlider   the page slider
     */
    public TabListListener(AbilitySlice abilitySlice, List<? extends TabInfo> list, PageSlider pageSlider) {
        this.tabViews = list;
        this.pager = pageSlider;
    }

    /**
     * onTabSelected
     *
     * @param tab the TabList.tab
     */
    public void onSelected(TabList.Tab tab) {
        if (tab == null) {
            return;
        }
        PageSlider pageSlider = pager;
        if (pageSlider != null) {
            pageSlider.setCurrentPage(tab.getPosition());
        }
    }

    /**
     * On Tab UnSelected
     *
     * @param tab the TabList.tab
     */
    public void onUnselected(TabList.Tab tab) {
        if (tab == null) {
            Logs.e(TAG, "onTabUnselected tab is null");
        }
    }


    private Optional<TabInfo> getAssociateTabView(TabList.Tab tab) {
        int position = tab.getPosition();
        if (position >= 0 && position <= tabViews.size()) {
            return Optional.ofNullable((TabInfo) tabViews.get(position));
        }
        Logs.e(TAG, "tab position error");
        return Optional.empty();
    }

    /**
     * On Tab Reselected
     *
     * @param tab the TabList.tab
     */
    public void onReselected(TabList.Tab tab) {
        Logs.i(TAG, " onTabReselected");
    }
}