package com.mobile.mobilehardware.simcard;

import com.mobile.mobilehardware.utils.TextUtil;
import ohos.app.Context;
import ohos.telephony.CellularDataInfoManager;
import ohos.telephony.RadioInfoManager;
import ohos.telephony.SimInfoManager;

import static com.mobile.mobilehardware.simcard.SimCardInfo.hasSimCardInSlot;
import static com.mobile.mobilehardware.simcard.SimCardUtils.getActiveSIMSlot;
import static ohos.telephony.TelephonyConstants.SIM_STATE_LOADED;
import static ohos.telephony.TelephonyConstants.SIM_STATE_READY;

/**
 * @author guxiaonian
 */
public class MobCardUtils {

    private static final String TAG = MobCardUtils.class.getSimpleName();

    private static final String CM_MOBILE1 = "46000";
    private static final String CM_MOBILE2 = "46002";
    private static final String CM_MOBILE3 = "46004";
    private static final String CM_MOBILE4 = "46007";
    private static final String CU_MOBILE1 = "46001";
    private static final String CU_MOBILE2 = "46006";
    private static final String CU_MOBILE3 = "46009";
    private static final String CT_MOBILE1 = "46003";
    private static final String CT_MOBILE2 = "46005";
    private static final String CT_MOBILE3 = "46011";

    /**
     * 获取网络运营商，CM是移动，CU是联通，CT是电信
     *
     * @param data str
     * @return str
     */
    private static String getOperators(String data) {
        if (!TextUtil.isEmpty(data)) {
            if (data.startsWith(CM_MOBILE1) || data.startsWith(CM_MOBILE2) || data.startsWith(CM_MOBILE3) || data.startsWith(CM_MOBILE4)) {
                return "CM";
            } else if (data.startsWith(CU_MOBILE1) || data.startsWith(CU_MOBILE2) || data.startsWith(CU_MOBILE3)) {
                return "CU";
            } else if (data.startsWith(CT_MOBILE1) || data.startsWith(CT_MOBILE2) || data.startsWith(CT_MOBILE3)) {
                return "CT";
            } else {
                return null;
            }
        } else {
            return null;
        }

    }

    /**
     * mobile info
     * @param simCardBean Storage bean
     * @param context Application context
     * @return sim card info
     */
    public static void mobGetCardInfo(Context context, SimCardBean simCardBean) {
        SimInfoManager simInfoManager = SimInfoManager.getInstance(context);
        int simStub = getDefaultDataSub(context);
        simCardBean.setSimSlotIndex(simStub);
        boolean sim1Ready = getSIMStateBySlot(context, 0);
        boolean sim2Ready = getSIMStateBySlot(context, 1);

        simCardBean.setSim1Ready(sim1Ready);
        simCardBean.setSim2Ready(sim2Ready);
        simCardBean.setTwoCard((sim1Ready && sim2Ready));

        String sim1Imei = null;
        String sim2Imei = null;
        RadioInfoManager radioInfoManager = RadioInfoManager.getInstance(context);
        sim1Imei = radioInfoManager.getImei(0);
        sim2Imei = radioInfoManager.getImei(1);
        if (!TextUtil.isEmpty(sim1Imei)) {
            simCardBean.setSim1Imei(MidInfo.isNumeric(sim1Imei) ? sim1Imei : null);
        }
        if (!TextUtil.isEmpty(sim2Imei)) {
            simCardBean.setSim2Imei(MidInfo.isNumeric(sim2Imei) ? sim2Imei : null);
        }
        String sim1Operator = getOperators(simInfoManager.getSimOperatorNumeric(0));
        String sim2Operator = getOperators(simInfoManager.getSimOperatorNumeric(1));

        simInfoQuery(context, simCardBean);

        simCardBean.setSim1ImsiOperator(sim1Operator);

        if (TextUtil.isEmpty(sim2Operator)) {
            if (TextUtil.isEmpty(simCardBean.getSim2carrierName())) {
                simCardBean.setSim2ImsiOperator(getIccidOperators(simCardBean.getSim2IccId()));
            } else {
                simCardBean.setSim2ImsiOperator(getCarrierOperators(simCardBean.getSim2carrierName()));
            }
        } else {
            simCardBean.setSim2ImsiOperator(sim2Operator);
        }

        simCardBean.setOperator(simCardBean.getSimSlotIndex() == 0 ? simCardBean.getSim1ImsiOperator() : simCardBean.getSim2ImsiOperator());
        if (TextUtil.isEmpty(simCardBean.getOperator())) {
            simCardBean.setOperator(radioInfoManager.getOperatorName(0));
        }
    }

    private static String getIccidOperators(String data) {
        if (!TextUtil.isEmpty(data)) {
            if (data.startsWith("898600") || data.startsWith("898602") || data.startsWith("898604") || data.startsWith("898607")) {
                return "CM";
            } else if (data.startsWith("898601") || data.startsWith("898606") || data.startsWith("898609")) {
                return "CU";
            } else if (data.startsWith("898603") || data.startsWith("898611")) {
                return "CT";
            } else {
                return null;
            }
        } else {
            return null;
        }

    }

    private static String getCarrierOperators(String data) {
        if (!TextUtil.isEmpty(data)) {
            if (data.startsWith("中国移动")) {
                return "CM";
            } else if (data.startsWith("中国联通")) {
                return "CU";
            } else if (data.startsWith("中国电信")) {
                return "CT";
            } else {
                return null;
            }
        } else {
            return null;
        }

    }

    private static void simInfoQuery(Context context, SimCardBean simCardBean) {
        SimInfoManager simInfoManager = SimInfoManager.getInstance(context);
        boolean sim1ready = false;
        boolean sim2ready = false;
        if (getActiveSIMCount(context) == 1) {
            if (simInfoManager.getMaxSimCount() == 1)
                sim1ready = true;
            else {
                if (getActiveSIMSlot(context) == 0) {
                    sim1ready = true;
                } else {
                    sim2ready = true;
                }
            }
        } else {
            sim1ready = true;
            sim2ready = true;
        }
        if (sim1ready) {
            simCardBean.setSim1mcc(mcc(simInfoManager.getSimOperatorNumeric(0)));
            simCardBean.setSim1mnc(mnc(simInfoManager.getSimOperatorNumeric(0)));
            simCardBean.setSim1carrierName(simInfoManager.getSimSpn(0));
        }
        if (sim2ready) {
            simCardBean.setSim1mcc(mcc(simInfoManager.getSimOperatorNumeric(1)));
            simCardBean.setSim1mnc(mnc(simInfoManager.getSimOperatorNumeric(1)));
            simCardBean.setSim1carrierName(simInfoManager.getSimSpn(1));
        }
    }

    private static int getActiveSIMCount(Context context) {
        int activesims = 0;
        SimInfoManager simInfoManager = SimInfoManager.getInstance(context);
        for (int i = 0; i < simInfoManager.getMaxSimCount(); i++) {
            if (hasSimCardInSlot(context, i)) {
                activesims++;
            }
        }
        return activesims;
    }

    public static String mcc(String plmnId) {
        if (plmnId !=null && plmnId.length() >= 3)
            return plmnId.substring(0, 3);
        else
            return "";
    }

    public static String mnc(String plmnId) {
        if (plmnId !=null && plmnId.length() >= 3)
            return plmnId.substring(3);
        else
            return "";
    }

    /**
     * 判断卡是否已经准备好
     *
     * @param slotID slot id
     * @param context Application Context
     * @return get sim state for slot
     */
    private static boolean getSIMStateBySlot(Context context, int slotID) {
        int simState = SimInfoManager.getInstance(context).getSimState(slotID);
        return (simState == SIM_STATE_READY) || (simState == SIM_STATE_LOADED);
    }


    /**
     * 获取哪张卡开启的运营商
     *
     * @param context con
     * @return int
     */
    private static int getDefaultDataSub(Context context) {
        return CellularDataInfoManager.getInstance(context).getDefaultCellularDataSlotId();
    }
}
