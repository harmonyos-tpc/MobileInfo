package com.mobile.mobileinfo.view.page;

import com.mobile.mobileinfo.data.Param;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class HookInfoOhosTest {
    HookInfo mHookInfo;
    List<Param> list;

    @Test
    public void getList() {
        list = mHookInfo.getList();
        assertTrue(list.size() > 0);
    }

    @Test
    public void getName() {
        assertEquals("HookInfo", mHookInfo.getName());
    }

    @Before
    public void setUp() {
        mHookInfo = new HookInfo();
    }
}