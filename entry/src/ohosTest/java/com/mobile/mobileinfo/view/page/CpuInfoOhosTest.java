package com.mobile.mobileinfo.view.page;

import com.mobile.mobilehardware.base.BaseData;
import com.mobile.mobileinfo.data.Param;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static com.mobile.mobileinfo.view.page.Util.getParamValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class CpuInfoOhosTest {
    CpuInfo mCpuInfo;
    List<Param> list;

    @Before
    public void setUp() {
        mCpuInfo = new CpuInfo();
    }

    @Test
    public void getName() {
        assertEquals("CpuInfo", mCpuInfo.getName());
    }

    @Test
    public void getList() {
        list = mCpuInfo.getList();
        assertTrue(getParamValue(list, BaseData.Cpu.CPU_NAME).length() > 0);
        assertTrue(getParamValue(list, BaseData.Cpu.CPU_PART).length() > 0);
        assertTrue(getParamValue(list, BaseData.Cpu.BOGO_MIPS).length() > 0);
        assertTrue(getParamValue(list, BaseData.Cpu.FEATURES).length() > 0);
        assertTrue(getParamValue(list, BaseData.Cpu.CPU_IMPLEMENTER).length() > 0);
        assertTrue(getParamValue(list, BaseData.Cpu.CPU_ARCHITECTURE).length() > 0);
        assertTrue(getParamValue(list, BaseData.Cpu.CPU_VARIANT).length() > 0);
        assertTrue(getParamValue(list, BaseData.Cpu.CPU_FREQ).length() > 0);
        assertTrue(getParamValue(list, BaseData.Cpu.CPU_MAX_FREQ).length() > 0);
        assertTrue(getParamValue(list, BaseData.Cpu.CPU_MIN_FREQ).length() > 0);
        assertTrue(getParamValue(list, BaseData.Cpu.CPU_HARDWARE).length() > 0);
        assertTrue(getParamValue(list, BaseData.Cpu.CPU_CORES).length() > 0);
        assertTrue(getParamValue(list, BaseData.Cpu.CPU_TEMP).length() > 0);
        assertTrue(getParamValue(list, BaseData.Cpu.CPU_ABI).length() > 0);


    }
}