package com.mobile.mobilehardware.screen;

import com.mobile.mobilehardware.base.BaseBean;
import com.mobile.mobilehardware.base.BaseData;

import com.mobile.mobilehardware.utils.Logs;
import ohos.utils.zson.ZSONObject;

/**
 * @author guxiaonian
 */
public class ScreenBean extends BaseBean {
    private static final String TAG = ScreenBean.class.getSimpleName();

    /**
     * 当前屏幕密度与标准屏幕密度的比值
     */
    private float densityScale;

    /**
     * 屏幕密度
     */
    private int densityDpi;

    /**
     * 屏幕宽度
     */
    private int width;

    /**
     * 屏幕高度
     */
    private int height;

    /**
     * 亮度是否为自动调节
     */
    private boolean isScreenAuto;

    /**
     * 屏幕亮度
     */
    private int screenBrightness;

    /**
     * 屏幕是否开启自动旋转
     */
    private boolean isScreenAutoChange;

    /**
     * 是否隐藏状态栏
     */
    private boolean checkHideStatusBar;

    public float getDensityScale() {
        return densityScale;
    }

    public void setDensityScale(float densityScale) {
        this.densityScale = densityScale;
    }

    public int getDensityDpi() {
        return densityDpi;
    }

    public void setDensityDpi(int densityDpi) {
        this.densityDpi = densityDpi;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public boolean isScreenAuto() {
        return isScreenAuto;
    }

    public void setScreenAuto(boolean screenAuto) {
        isScreenAuto = screenAuto;
    }

    public int getScreenBrightness() {
        return screenBrightness;
    }

    public void setScreenBrightness(int screenBrightness) {
        this.screenBrightness = screenBrightness;
    }

    public boolean isScreenAutoChange() {
        return isScreenAutoChange;
    }

    public void setScreenAutoChange(boolean screenAutoChange) {
        isScreenAutoChange = screenAutoChange;
    }

    public boolean isCheckHideStatusBar() {
        return checkHideStatusBar;
    }

    public void setCheckHideStatusBar(boolean checkHideStatusBar) {
        this.checkHideStatusBar = checkHideStatusBar;
    }

    @Override
    protected ZSONObject toJSONObject() {
        try {
            jsonObject.put(BaseData.Screen.DENSITY_SCALE, densityScale);
            jsonObject.put(BaseData.Screen.DENSITY_DPI, densityDpi);
            jsonObject.put(BaseData.Screen.WIDTH, width);
            jsonObject.put(BaseData.Screen.HEIGHT, height);
            jsonObject.put(BaseData.Screen.IS_SCREEN_AUTO, isScreenAuto);
            jsonObject.put(BaseData.Screen.IS_SCREEN_AUTO_CHANGE, isScreenAutoChange);
            jsonObject.put(BaseData.Screen.SCREEN_BRIGHTNESS, screenBrightness);
            jsonObject.put(BaseData.Screen.CHECK_HIDE_STATUSBAR, checkHideStatusBar);
        } catch (Exception e) {
            Logs.e(TAG, e.toString());
        }
        return super.toJSONObject();
    }
}
