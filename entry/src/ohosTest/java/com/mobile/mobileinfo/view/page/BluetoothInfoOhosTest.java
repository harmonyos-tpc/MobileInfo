package com.mobile.mobileinfo.view.page;

import com.mobile.mobilehardware.base.BaseData;
import com.mobile.mobileinfo.data.Param;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static com.mobile.mobileinfo.view.page.Util.getParamValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class BluetoothInfoOhosTest {
    BluetoothInfo mBluetoothInfo;
    List<Param> list;

    @Test
    public void getList() {
        list = mBluetoothInfo.getList();
        assertTrue(getParamValue(list, BaseData.Bluetooth.IS_ENABLED).length() > 0);
        assertTrue(getParamValue(list, BaseData.Bluetooth.DEVICE).length() > 0);
        assertTrue(getParamValue(list, BaseData.Bluetooth.PHONE_NAME).length() > 0);
    }

    @Test
    public void getName() {
        assertEquals("BluetoothInfo", mBluetoothInfo.getName());
    }

    @Before
    public void setUp() {
        mBluetoothInfo = new BluetoothInfo();
    }
}