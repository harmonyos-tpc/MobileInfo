package com.mobile.mobilehardware.network;

import com.mobile.mobilehardware.MobileHardWareHelper;
import ohos.utils.zson.ZSONObject;

public class NetWorkHelper extends NetWorkInfo{
    public static ZSONObject mobGetMobNetWork() {
        return getMobNetWork(MobileHardWareHelper.getContext());
    }
}
