package com.mobile.mobilehardware.moreopen;

import com.mobile.mobilehardware.MobileHardWareHelper;
import ohos.utils.zson.ZSONObject;


/**
 * @author guxiaonian
 */
public class MoreOpenHelper extends MoreOpenInfo {

    public static ZSONObject checkVirtual() {
        return checkVirtualInfo(MobileHardWareHelper.getContext());
    }

}
