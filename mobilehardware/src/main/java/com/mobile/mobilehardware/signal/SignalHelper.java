package com.mobile.mobilehardware.signal;


import com.mobile.mobilehardware.MobileHardWareHelper;
import ohos.utils.zson.ZSONObject;


/**
 * @author guxiaonian
 */
public class SignalHelper extends SignalInfo {


    /**
     * 信号强度获取
     *
     * @return json object for signal
     */
    public static ZSONObject mobGetNetRssi() {
        return getNetRssi(MobileHardWareHelper.getContext());
    }


}
