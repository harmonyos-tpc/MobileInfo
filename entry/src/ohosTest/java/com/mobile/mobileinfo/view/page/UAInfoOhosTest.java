package com.mobile.mobileinfo.view.page;

import com.mobile.mobilehardware.base.BaseData;
import com.mobile.mobilehardware.useragent.UserAgentHelper;
import com.mobile.mobileinfo.data.Param;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static com.mobile.mobileinfo.view.page.Util.getParamValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class UAInfoOhosTest {
    UAInfo mUAInfo;
    List<Param> list;

    @Before
    public void setUp() throws Exception {
        mUAInfo = new UAInfo();
    }

    @Test
    public void getName() {
        assertEquals("UAInfo", mUAInfo.getName());
    }

    @Test
    public void getList() {
        list = mUAInfo.getList();
        assertTrue(getParamValue(list, "userAgent").length() > 0);
    }
}