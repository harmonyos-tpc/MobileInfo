package com.mobile.mobilehardware.battery;

import com.mobile.mobilehardware.base.BaseData;
import ohos.batterymanager.BatteryInfo.BatteryChargeState;
import ohos.batterymanager.BatteryInfo.BatteryHealthState;
import ohos.batterymanager.BatteryInfo.BatteryPluggedType;
import ohos.utils.zson.ZSONObject;

/**
 * @author guxiaonian
 */
class BatteryInfo {

    static ZSONObject getBattery() {
        BatteryBean batteryBean = new BatteryBean();

        ohos.batterymanager.BatteryInfo bi = new ohos.batterymanager.BatteryInfo();
        int temperature = bi.getBatteryTemperature();
        int voltage = bi.getVoltage();
        batteryBean.setBr(bi.getCapacity() + "%");
        batteryBean.setStatus(batteryStatus(bi.getChargingStatus().ordinal()));
        batteryBean.setPlugState(batteryPlugged(bi.getPluggedType().ordinal()));
        batteryBean.setHealth(batteryHealth(bi.getHealthStatus().ordinal()));
        batteryBean.setPresent(bi.getPresent());
        batteryBean.setTechnology(bi.getTechnology());
        batteryBean.setTemperature(temperature / 10 + "℃");
        if (voltage > 1000) {
            batteryBean.setVoltage(voltage / 1000f + "V");
        } else {
            batteryBean.setVoltage(voltage + "V");
        }
        return batteryBean.toJSONObject();
    }

    private static String batteryHealth(int health) {
        if (health == BatteryHealthState.COLD.ordinal()) {
            return "cold";
        }
        if (health == BatteryHealthState.DEAD.ordinal()) {
            return "dead";
        }
        if (health == BatteryHealthState.GOOD.ordinal()) {
            return "good";
        }
        if (health == BatteryHealthState.OVERVOLTAGE.ordinal()) {
            return "overVoltage";
        }
        if (health == BatteryHealthState.OVERHEAT.ordinal()) {
            return "overheat";
        }
        if (health == BatteryHealthState.UNKNOWN.ordinal()) {
            return "unknown";
        }
        if (health == BatteryHealthState.RESERVED.ordinal()) {
            return "reserved";
        }
        return BaseData.UNKNOWN_PARAM;
    }

    private static String batteryStatus(int status) {
        if (status == BatteryChargeState.ENABLE.ordinal()) {
            return "charging";
        }
        if (status == BatteryChargeState.DISABLE.ordinal()) {
            return "notCharging";
        }
        if (status == BatteryChargeState.FULL.ordinal()) {
            return "full";
        }
        if (status == BatteryChargeState.NONE.ordinal()) {
            return BaseData.UNKNOWN_PARAM;
        }
        if (status == BatteryChargeState.RESERVED.ordinal()) {
            return "reserved";
        }
        return BaseData.UNKNOWN_PARAM;
    }

    private static String batteryPlugged(int status) {
        if (status == BatteryPluggedType.AC.ordinal()) {
            return "ac";
        } else if (status == BatteryPluggedType.USB.ordinal()) {
            return "usb";
        } else if (status == BatteryPluggedType.WIRELESS.ordinal()) {
            return "wireless";
        }
        return BaseData.UNKNOWN_PARAM;
    }
}
