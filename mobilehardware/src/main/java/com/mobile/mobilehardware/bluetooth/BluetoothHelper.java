package com.mobile.mobilehardware.bluetooth;


import com.mobile.mobilehardware.MobileHardWareHelper;
import ohos.utils.zson.ZSONObject;


/**
 * @author guxiaonian
 */
public class BluetoothHelper extends BluetoothInfo {

    /**
     * 获取蓝牙信息
     * @return BT info
     */
    public static ZSONObject mobGetMobBluetooth() {
        return getMobBluetooth(MobileHardWareHelper.getContext());
    }


}
