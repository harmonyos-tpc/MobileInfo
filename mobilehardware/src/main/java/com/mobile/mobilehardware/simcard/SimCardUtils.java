package com.mobile.mobilehardware.simcard;

import ohos.app.Context;
import ohos.telephony.CellularDataInfoManager;
import ohos.telephony.RadioInfoManager;
import ohos.telephony.SimInfoManager;

import static com.mobile.mobilehardware.simcard.SimCardInfo.hasSimCardInSlot;

/**
 * @author guxiaonian
 */

public class SimCardUtils {
    private static final String TAG = "SimCardUtils";
    private static volatile SimCardUtils mGopSimCardUtils;
    private SimCardInfo mSimCardInfo = null;
    private static int mActiveSIMSlot = -1;

    private SimCardUtils() {
    }

    public static SimCardUtils instance() {
        if (mGopSimCardUtils == null) {
            mGopSimCardUtils = new SimCardUtils();
        }

        return mGopSimCardUtils;
    }

    public SimCardInfo getmSimCardInfo(Context context) {
        this.getmGopSimCardUtils(context);
        return this.mSimCardInfo;
    }

    private SimCardUtils getmGopSimCardUtils(Context context) {
        this.mSimCardInfo = new SimCardInfo();
        this.collectionSimInfoNewVersion(context);
        this.getDefaultSub(context);
        this.getDefaultSimState(context);
        return this;

    }

    private void getDefaultSub(Context context) {
        CellularDataInfoManager cellularDataInfoManager = CellularDataInfoManager.getInstance(context);
        this.mSimCardInfo.simSub = cellularDataInfoManager.getDefaultCellularDataSlotId();
    }

    private void getDefaultSimState(Context context) {
        if (ohos.wifi.WifiDevice.getInstance(context).isConnected())
            this.mSimCardInfo.simState = 0;
        else if (SimInfoManager.getInstance(context).getDefaultVoiceSlotId() != 1) {
            this.mSimCardInfo.simState = 1;
        } else{
            this.mSimCardInfo.simState = -1;
        }
    }

    private void collectionSimInfoNewVersion(Context context) {
        SimInfoManager simInfoManager = SimInfoManager.getInstance(context);
        RadioInfoManager radioInfoManager = RadioInfoManager.getInstance(context);
        if (getActiveSIMCount(context) == 1) {
            if (simInfoManager.getMaxSimCount() == 1)
                this.deviceInfo(radioInfoManager, simInfoManager);
            else {
                if (getActiveSIMSlot(context) == 0) {
                    this.deviceInfo(radioInfoManager, simInfoManager);
                } else {
                    this.deviceInfoTwo(radioInfoManager, simInfoManager);
                }
            }
        } else {
            this.deviceInfo(radioInfoManager, simInfoManager);
            this.deviceInfoTwo(radioInfoManager, simInfoManager);
        }
    }

    private int getActiveSIMCount(Context context) {
        int activesims = 0;
        SimInfoManager simInfoManager = SimInfoManager.getInstance(context);
        for (int i=0; i < simInfoManager.getMaxSimCount(); i++) {
            if (hasSimCardInSlot(context, i)) {
                activesims++;
            }
        }
        return activesims;
    }

    public static int getActiveSIMSlot(Context context) {
        if (mActiveSIMSlot == -1) {
            SimInfoManager simInfoManager = SimInfoManager.getInstance(context);
            int activesimslot = simInfoManager.getDefaultVoiceSlotId();
            if (activesimslot != -1) {
                mActiveSIMSlot = activesimslot;
            } else {
                for (int i=0; i < simInfoManager.getMaxSimCount(); i++) {
                    if (hasSimCardInSlot(context, i)) {
                        mActiveSIMSlot = i;
                        break;
                    }
                }
            }
        }
        return mActiveSIMSlot;
    }

    private void deviceInfo(RadioInfoManager radioInfoManager, SimInfoManager simInfoManager) {
        this.mSimCardInfo.setSim1SlotIndex(0);
        this.mSimCardInfo.setSim1Imei(radioInfoManager.getImei(0));
        this.mSimCardInfo.sim1State = true;
        this.mSimCardInfo.setSim1SimOperator(simInfoManager.getSimSpn(0));
    }

    private void deviceInfoTwo(RadioInfoManager radioInfoManager, SimInfoManager simInfoManager) {
        this.mSimCardInfo.setSim2Imei(radioInfoManager.getImei(1));
        this.mSimCardInfo.sim2State = true;
        this.mSimCardInfo.setSim2SlotIndex(1);
        this.mSimCardInfo.setSim2SimOperator(simInfoManager.getSimSpn(1));
    }

    public static class SimCardInfo {
        private String sim1Imei = "";
        private String sim2Imei = "";
        private String sim1Imsi = "";
        private String sim2Imsi = "";
        private boolean sim1State = false;
        private boolean sim2State = false;
        private int sim1SlotIndex = -1;
        private int sim2SlotIndex = -1;
        private int sim1SubscriptionId = -1;
        private int sim2SubscriptionId = -1;
        private String sim1SimOperator = "";
        private String sim2SimOperator = "";
        private int simSub = -1;
        private int simState = -1;

        public SimCardInfo() {
        }

        @Override
        public String toString() {
            return "SimCardInfo{" +
                    "sim1Imei='" + sim1Imei + '\'' +
                    ", sim2Imei='" + sim2Imei + '\'' +
                    ", sim1Imsi='" + sim1Imsi + '\'' +
                    ", sim2Imsi='" + sim2Imsi + '\'' +
                    ", sim1State=" + sim1State +
                    ", sim2State=" + sim2State +
                    ", sim1SlotIndex=" + sim1SlotIndex +
                    ", sim2SlotIndex=" + sim2SlotIndex +
                    ", sim1SubscriptionId=" + sim1SubscriptionId +
                    ", sim2SubscriptionId=" + sim2SubscriptionId +
                    ", sim1SimOperator='" + sim1SimOperator + '\'' +
                    ", sim2SimOperator='" + sim2SimOperator + '\'' +
                    ", simSub=" + simSub +
                    ", simState=" + simState +
                    '}';
        }

        public int getSimState() {
            return simState;
        }

        public void setSimState(int simState) {
            this.simState = simState;
        }

        public String getSim1Imei() {
            return sim1Imei;
        }

        public void setSim1Imei(String sim1Imei) {
            this.sim1Imei = sim1Imei;
        }

        public String getSim2Imei() {
            return sim2Imei;
        }

        public void setSim2Imei(String sim2Imei) {
            this.sim2Imei = sim2Imei;
        }

        public String getSim1Imsi() {
            return sim1Imsi;
        }

        public void setSim1Imsi(String sim1Imsi) {
            this.sim1Imsi = sim1Imsi;
        }

        public String getSim2Imsi() {
            return sim2Imsi;
        }

        public void setSim2Imsi(String sim2Imsi) {
            this.sim2Imsi = sim2Imsi;
        }

        public boolean isSim1State() {
            return sim1State;
        }

        public void setSim1State(boolean sim1State) {
            this.sim1State = sim1State;
        }

        public boolean isSim2State() {
            return sim2State;
        }

        public void setSim2State(boolean sim2State) {
            this.sim2State = sim2State;
        }

        public int getSim1SlotIndex() {
            return sim1SlotIndex;
        }

        public void setSim1SlotIndex(int sim1SlotIndex) {
            this.sim1SlotIndex = sim1SlotIndex;
        }

        public int getSim2SlotIndex() {
            return sim2SlotIndex;
        }

        public void setSim2SlotIndex(int sim2SlotIndex) {
            this.sim2SlotIndex = sim2SlotIndex;
        }

        public int getSim1SubscriptionId() {
            return sim1SubscriptionId;
        }

        public void setSim1SubscriptionId(int sim1SubscriptionId) {
            this.sim1SubscriptionId = sim1SubscriptionId;
        }

        public int getSim2SubscriptionId() {
            return sim2SubscriptionId;
        }

        public void setSim2SubscriptionId(int sim2SubscriptionId) {
            this.sim2SubscriptionId = sim2SubscriptionId;
        }

        public String getSim1SimOperator() {
            return sim1SimOperator;
        }

        public void setSim1SimOperator(String sim1SimOperator) {
            this.sim1SimOperator = sim1SimOperator;
        }

        public String getSim2SimOperator() {
            return sim2SimOperator;
        }

        public void setSim2SimOperator(String sim2SimOperator) {
            this.sim2SimOperator = sim2SimOperator;
        }

        public int getSimSub() {
            return simSub;
        }

        public void setSimSub(int simSub) {
            this.simSub = simSub;
        }


        public String getImsi(int num) {
            return this.sim1SlotIndex == num ? this.sim1Imsi : (this.sim2SlotIndex == num ? this.sim2Imsi : "");
        }

        public String getImei(int num) {
            return this.sim1SlotIndex == num ? this.sim1Imei : (this.sim2SlotIndex == num ? this.sim2Imei : "");
        }

        public String getOperator(int num) {
            return this.sim1SlotIndex == num ? this.sim1SimOperator : (this.sim2SlotIndex == num ? this.sim2SimOperator : "");
        }
    }
}
